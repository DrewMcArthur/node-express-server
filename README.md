node-express-server
===================

An attempt at a web server using Node.js and Express

A How To for the setup. 

1. Install node.js, npm, and git

    `apt-get install nodejs npm git`

2. install express frameworks

    `npm install -g express`

3. clone into repository found [here](https://www.github.com/DrewMcArthur/node-express-server)

    `git clone git@github.com:DrewMcArthur/node-express-server.git`

4. in directory, run npm install to install dependencies

    `npm install`

5. run server with nodejs app.js 

    `nodejs app.js`

6. …?

7. Profit!

8. Your Web Server is now running at [`http://localhost:1337`](http://localhost:1337)

NOTE:  some commands may require root privelages, i.e. `sudo`.  See 1,2, and 4
